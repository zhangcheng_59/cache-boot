package com.zc.cacheboot.service;

import com.zc.cacheboot.param.CachePutParam;
import com.zc.cacheboot.param.CacheableParam;

/**
 * Project：cache-boot
 * Date：2022/3/11
 * Time：13:51
 * Description：缓存操作service
 *
 * @author Challen.Zhang
 * @version 1.0
 */
public interface CacheBootOperationService {

    /**
     * 可在此增加布隆过滤器
     *
     * @param key 缓存key
     * @return 是否存在
     */
    default boolean isExist(String key) {
        return true;
    }

    /**
     * 具体实现get方法
     *
     * @param cacheableParam cacheable相关参数
     * @return 执行结果
     */
    <T> T invoke(CacheableParam<T> cacheableParam);


    /**
     * 具体实现put方法
     *
     * @param cachePutParam put相关参数
     * @return 执行结果
     */
   Object invoke(CachePutParam cachePutParam);

    /**
     * 清除缓存
     * @param key       缓存key
     */
    void delete(String key);

}
