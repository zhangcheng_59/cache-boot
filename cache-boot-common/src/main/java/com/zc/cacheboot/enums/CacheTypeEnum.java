package com.zc.cacheboot.enums;

/**
 * Project：cache-boot
 * Date：2022-03-05
 * Time：9:49
 * Description：缓存类型枚举
 *
 * @author ChallenZhang
 * @version 1.0
 */
public enum CacheTypeEnum {
    /**
     * 使用redis缓存
     */
    REDIS,
    ;
}
