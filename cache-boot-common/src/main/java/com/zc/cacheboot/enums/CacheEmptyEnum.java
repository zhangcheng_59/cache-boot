package com.zc.cacheboot.enums;

import lombok.Getter;

/**
 * Project：cache-boot
 * Date：2022/3/8
 * Time：14:08
 * Description：空值枚举
 *
 * @author Challen.Zhang
 * @version 1.0
 */
@Getter
public enum CacheEmptyEnum {

    /**
     * 空值
     */
    EMPTY_VAL(""),

    /**
     * 空key
     */
    EMPTY_KEY("EMPTY_CACHE:")
    ;


    private final String val;

    CacheEmptyEnum(String val) {
        this.val = val;
    }
}
