package com.zc.cacheboot.constant;

import com.zc.cacheboot.annotation.CacheEvict;
import com.zc.cacheboot.annotation.CachePut;
import com.zc.cacheboot.annotation.Cacheable;

import java.lang.annotation.Annotation;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Project：cache-boot
 * Date：2022/3/11
 * Time：16:42
 * Description：存储注解集合
 *
 * @author Challen.Zhang
 * @version 1.0
 */
public class CacheAnnotationSet {
    private static final Set<Class<? extends Annotation>> CACHE_OPERATION_ANNOTATIONS = new LinkedHashSet<>(8);

    static {
        CACHE_OPERATION_ANNOTATIONS.add(Cacheable.class);
        CACHE_OPERATION_ANNOTATIONS.add(CacheEvict.class);
        CACHE_OPERATION_ANNOTATIONS.add(CachePut.class);
    }

    public static Set<Class<? extends Annotation>> getCacheOperationAnnotations() {
        return CACHE_OPERATION_ANNOTATIONS;
    }

}
