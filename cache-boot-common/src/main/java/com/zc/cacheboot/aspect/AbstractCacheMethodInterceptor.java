package com.zc.cacheboot.aspect;

import com.zc.cacheboot.config.CacheBootProperties;
import com.zc.cacheboot.enums.CacheEmptyEnum;
import com.zc.cacheboot.parser.CacheBootExpressionParser;
import com.zc.cacheboot.service.CacheBootOperationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cglib.proxy.MethodInterceptor;

import java.lang.reflect.Method;

/**
 * Project：cache-boot
 * Date：2022-03-05
 * Time：11:54
 * Description：抽象的增强操作
 *
 * @author ChallenZhang
 * @version 1.0
 */
@Slf4j
public abstract class AbstractCacheMethodInterceptor implements MethodInterceptor {

    public AbstractCacheMethodInterceptor() {
    }

    public AbstractCacheMethodInterceptor(Object bean) {
        this.bean = bean;
    }

    public AbstractCacheMethodInterceptor(Object bean, CacheBootProperties cacheBootProperties) {
        this.bean = bean;
        this.cacheBootProperties = cacheBootProperties;
    }

    public AbstractCacheMethodInterceptor(Object bean, CacheBootProperties cacheBootProperties, CacheBootOperationService cacheBootOperationService) {
        this.bean = bean;
        this.cacheBootProperties = cacheBootProperties;
        this.cacheBootOperationService = cacheBootOperationService;
    }

    protected Object bean;

    protected CacheBootProperties cacheBootProperties;

    protected CacheBootOperationService cacheBootOperationService;

    public Object getBean() {
        return bean;
    }

    public void setBean(Object bean) {
        this.bean = bean;
    }
}
