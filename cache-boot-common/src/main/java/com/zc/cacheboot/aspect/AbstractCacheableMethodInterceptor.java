package com.zc.cacheboot.aspect;

import com.zc.cacheboot.annotation.Cacheable;
import com.zc.cacheboot.config.CacheBootProperties;
import com.zc.cacheboot.listener.MethodAnnotationContext;
import com.zc.cacheboot.param.CacheableParam;
import com.zc.cacheboot.parser.CacheBootExpressionParser;
import com.zc.cacheboot.service.CacheBootOperationService;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * Project：cache-boot
 * Date：2022-03-05
 * Time：17:32
 * Description：增强Cacheable的拦截器
 *
 * @author ChallenZhang
 * @version 1.0
 */
public abstract class AbstractCacheableMethodInterceptor extends AbstractCacheMethodInterceptor {


    public AbstractCacheableMethodInterceptor() {
    }

    public AbstractCacheableMethodInterceptor(Object bean) {
        super(bean);
    }

    public AbstractCacheableMethodInterceptor(Object bean, CacheBootProperties cacheBootProperties) {
        super(bean, cacheBootProperties);
    }

    public AbstractCacheableMethodInterceptor(Object bean, CacheBootProperties cacheBootProperties, CacheBootOperationService cacheBootOperationService) {
        super(bean, cacheBootProperties, cacheBootOperationService);
    }


    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        Cacheable annotation = MethodAnnotationContext.getAnnotation(method);
        String expression = annotation.key();
        String keyGenerator = annotation.keyGenerator();
        String key = CacheBootExpressionParser.generateKey(expression, keyGenerator, o, method, cacheBootProperties, objects);
        if (!cacheBootOperationService.isExist(key)) {
            return null;
        }

        String condition = annotation.condition();
        boolean beforeCondition = CacheBootExpressionParser.parseCondition(condition, method, objects);
        if (!beforeCondition) {
            return methodProxy.invoke(bean, objects);
        }
        return cacheBootOperationService.invoke(new CacheableParam<>(bean, null, methodProxy, objects, key, annotation.expireTime(), annotation.unit(), annotation.unless(), method.getReturnType(), annotation.renewal()));
    }

}
