package com.zc.cacheboot.aspect;

import com.zc.cacheboot.annotation.CachePut;
import com.zc.cacheboot.config.CacheBootProperties;
import com.zc.cacheboot.listener.MethodAnnotationContext;
import com.zc.cacheboot.param.CachePutParam;
import com.zc.cacheboot.parser.CacheBootExpressionParser;
import com.zc.cacheboot.service.CacheBootOperationService;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * Project：cache-boot
 * Date：2022-03-05
 * Time：17:32
 * Description：增强CachePut的拦截器
 *
 * @author ChallenZhang
 * @version 1.0
 */
public abstract class AbstractCachePutMethodInterceptor extends AbstractCacheMethodInterceptor {
    public AbstractCachePutMethodInterceptor() {
    }

    public AbstractCachePutMethodInterceptor(Object bean) {
        super(bean);
    }

    public AbstractCachePutMethodInterceptor(Object bean, CacheBootProperties cacheBootProperties) {
        super(bean, cacheBootProperties);
    }

    public AbstractCachePutMethodInterceptor(Object bean, CacheBootProperties cacheBootProperties, CacheBootOperationService cacheBootOperationService) {
        super(bean, cacheBootProperties, cacheBootOperationService);
    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        CachePut annotation = MethodAnnotationContext.getAnnotation(method);
        String expression = annotation.key();
        String keyGenerator = annotation.keyGenerator();
        String key = CacheBootExpressionParser.generateKey(expression, keyGenerator, o, method, cacheBootProperties, objects);
        boolean beforeCondition = CacheBootExpressionParser.parseCondition(annotation.condition(), method, objects);
        if (!beforeCondition) {
            return methodProxy.invoke(bean, objects);
        }
        return cacheBootOperationService.invoke(new CachePutParam(bean, null, methodProxy, objects, key, annotation.expireTime(), annotation.unit(), annotation.unless()));
    }

}
