package com.zc.cacheboot.aspect;

import com.zc.cacheboot.annotation.CacheEvict;
import com.zc.cacheboot.annotation.CachePut;
import com.zc.cacheboot.annotation.Cacheable;
import com.zc.cacheboot.config.CacheBootProperties;
import com.zc.cacheboot.listener.MethodAnnotationContext;
import com.zc.cacheboot.param.CachePutParam;
import com.zc.cacheboot.parser.CacheBootExpressionParser;
import com.zc.cacheboot.service.CacheBootOperationService;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * Project：cache-boot
 * Date：2022/3/11
 * Time：14:51
 * Description：基于动态代理的增强操作
 *
 * @author Challen.Zhang
 * @version 1.0
 */
public abstract class AbstractCacheInvocationHandler implements InvocationHandler {

    protected Object bean;

    protected CacheBootProperties cacheBootProperties;

    protected CacheBootOperationService cacheBootOperationService;

    public AbstractCacheInvocationHandler() {
    }

    public AbstractCacheInvocationHandler(Object bean) {
        this.bean = bean;
    }

    public AbstractCacheInvocationHandler(Object bean, CacheBootProperties cacheBootProperties) {
        this.bean = bean;
        this.cacheBootProperties = cacheBootProperties;
    }

    public AbstractCacheInvocationHandler(Object bean, CacheBootProperties cacheBootProperties, CacheBootOperationService cacheBootOperationService) {
        this.bean = bean;
        this.cacheBootProperties = cacheBootProperties;
        this.cacheBootOperationService = cacheBootOperationService;
    }

    public Object getBean() {
        return bean;
    }

    public void setBean(Object bean) {
        this.bean = bean;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (MethodAnnotationContext.getAnnotation(method) == null) {
            return method.invoke(bean, args);
        }
        Annotation annotation = MethodAnnotationContext.getAnnotation(method);
        if (annotation instanceof Cacheable) {
            return invokeCacheable((Cacheable) annotation, proxy, method, args);
        }
        if (annotation instanceof CachePut) {
            return invokeCachePut((CachePut) annotation, proxy, method, args);
        }
        if (annotation instanceof CacheEvict) {
            return invokeCacheEvict((CacheEvict) annotation, proxy, method, args);
        }
        throw new RuntimeException();
    }

    /**
     * @param cacheable cacheable注解
     * @param proxy     代理对象
     * @param method    method
     * @param args      args 参数列表
     * @return 返回值
     * @throws Throwable 反射执行method异常
     */
    protected abstract Object invokeCacheable(Cacheable cacheable, Object proxy, Method method, Object[] args) throws Throwable;

    /**
     * 具体执行cachePut的逻辑
     *
     * @param cachePut cachePut注解
     * @param proxy    代理对象
     * @param method   method
     * @param args     args 参数列表
     * @return 返回值
     * @throws Throwable 反射执行method异常
     */
    protected abstract Object invokeCachePut(CachePut cachePut, Object proxy, Method method, Object[] args) throws Throwable;

    /**
     * 具体执行cacheEvict的逻辑
     *
     * @param cacheEvict cacheEvict注解
     * @param proxy      代理对象
     * @param method     method
     * @param args       args 参数列表
     * @return 返回值
     * @throws Throwable 反射执行method异常
     */
    protected abstract Object invokeCacheEvict(CacheEvict cacheEvict, Object proxy, Method method, Object[] args) throws Throwable;
}
