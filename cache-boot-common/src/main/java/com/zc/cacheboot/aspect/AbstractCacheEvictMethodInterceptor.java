package com.zc.cacheboot.aspect;

import com.zc.cacheboot.annotation.CacheEvict;
import com.zc.cacheboot.config.CacheBootProperties;
import com.zc.cacheboot.listener.MethodAnnotationContext;
import com.zc.cacheboot.parser.CacheBootExpressionParser;
import com.zc.cacheboot.service.CacheBootOperationService;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * Project：cache-boot
 * Date：2022-03-05
 * Time：17:32
 * Description：增强CachePut的拦截器
 *
 * @author ChallenZhang
 * @version 1.0
 */
public abstract class AbstractCacheEvictMethodInterceptor extends AbstractCacheMethodInterceptor {

    public AbstractCacheEvictMethodInterceptor() {
    }

    public AbstractCacheEvictMethodInterceptor(Object bean) {
        super(bean);
    }

    public AbstractCacheEvictMethodInterceptor(Object bean, CacheBootProperties cacheBootProperties) {
        super(bean, cacheBootProperties);
    }

    public AbstractCacheEvictMethodInterceptor(Object bean, CacheBootProperties cacheBootProperties, CacheBootOperationService cacheBootOperationService) {
        super(bean, cacheBootProperties, cacheBootOperationService);
    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        CacheEvict annotation = MethodAnnotationContext.getAnnotation(method);
        String key = CacheBootExpressionParser.generateKey(annotation.key(), annotation.keyGenerator(), o, method,cacheBootProperties, objects);
        if (! CacheBootExpressionParser.parseCondition(annotation.condition(), method, objects)) {
            return methodProxy.invoke(bean, objects);
        }

        if (annotation.beforeInvocation()) {
            cacheBootOperationService.delete(key);
            return methodProxy.invoke(bean, objects);
        }
        Object invoke = methodProxy.invoke(bean, objects);
        cacheBootOperationService.delete(key);
        return invoke;
    }


}
