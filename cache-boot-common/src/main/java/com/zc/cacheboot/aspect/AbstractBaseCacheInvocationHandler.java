package com.zc.cacheboot.aspect;

import com.zc.cacheboot.annotation.CacheEvict;
import com.zc.cacheboot.annotation.CachePut;
import com.zc.cacheboot.annotation.Cacheable;
import com.zc.cacheboot.config.CacheBootProperties;
import com.zc.cacheboot.param.CachePutParam;
import com.zc.cacheboot.param.CacheableParam;
import com.zc.cacheboot.parser.CacheBootExpressionParser;
import com.zc.cacheboot.service.CacheBootOperationService;

import java.lang.reflect.Method;

/**
 * Project：cache-boot
 * Date：2022-03-05
 * Time：17:32
 * Description：增强Cacheable的拦截器
 *
 * @author ChallenZhang
 * @version 1.0
 */
public abstract class AbstractBaseCacheInvocationHandler extends AbstractCacheInvocationHandler {


    public AbstractBaseCacheInvocationHandler() {
    }

    public AbstractBaseCacheInvocationHandler(Object bean) {
        super(bean);
    }

    public AbstractBaseCacheInvocationHandler(Object bean, CacheBootProperties cacheBootProperties) {
        super(bean, cacheBootProperties);
    }

    public AbstractBaseCacheInvocationHandler(Object bean, CacheBootProperties cacheBootProperties, CacheBootOperationService cacheBootOperationService) {
        super(bean, cacheBootProperties, cacheBootOperationService);
    }


    @Override
    protected Object invokeCacheable(Cacheable cacheable, Object proxy, Method method, Object[] args) throws Throwable {
        String keyGenerator = cacheable.keyGenerator();
        String key = CacheBootExpressionParser.generateKey(cacheable.key(), keyGenerator, proxy, method, cacheBootProperties, args);
        if (!cacheBootOperationService.isExist(key)) {
            return null;
        }
        String condition = cacheable.condition();
        boolean beforeCondition = CacheBootExpressionParser.parseCondition(condition, method, args);
        if (!beforeCondition) {
            return method.invoke(bean, args);
        }
        return cacheBootOperationService.invoke(new CacheableParam<>(bean, method, null, args, key, cacheable.expireTime(), cacheable.unit(), cacheable.unless(), method.getReturnType(), cacheable.renewal()));
    }


    @Override
    protected Object invokeCachePut(CachePut cachePut, Object proxy, Method method, Object[] args) throws Throwable {
        String key = CacheBootExpressionParser.generateKey(cachePut.key(), cachePut.keyGenerator(), proxy, method, cacheBootProperties, args);
        boolean beforeCondition = CacheBootExpressionParser.parseCondition(cachePut.condition(), method, args);
        if (!beforeCondition) {
            return method.invoke(bean, args);
        }
        return cacheBootOperationService.invoke(new CachePutParam(bean, method, null, args, key, cachePut.expireTime(), cachePut.unit(), cachePut.unless()));
    }


    @Override
    protected Object invokeCacheEvict(CacheEvict cacheEvict, Object proxy, Method method, Object[] args) throws Throwable {
        String expression = cacheEvict.key();
        String key = CacheBootExpressionParser.generateKey(expression, cacheEvict.keyGenerator(), proxy, method, cacheBootProperties, args);
        if (!CacheBootExpressionParser.parseCondition(cacheEvict.condition(), method, args)) {
            return method.invoke(bean, args);
        }
        if (cacheEvict.beforeInvocation()) {
            cacheBootOperationService.delete(key);
            return method.invoke(bean, args);
        }
        Object invoke = method.invoke(bean, args);
        cacheBootOperationService.delete(key);
        return invoke;
    }

}
