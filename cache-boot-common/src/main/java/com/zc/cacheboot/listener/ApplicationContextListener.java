package com.zc.cacheboot.listener;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * Project：cache-boot
 * Date：2022-03-06
 * Time：19:24
 * Description：spring-context监听
 *
 * @author ChallenZhang
 * @version 1.0
 */
@Component
public class ApplicationContextListener implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        ApplicationContextListener.applicationContext = applicationContext;
    }

    public static ApplicationContext getApplicationContext() {
        return ApplicationContextListener.applicationContext;
    }
}
