package com.zc.cacheboot.listener;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Project：cache-boot
 * Date：2022-03-06
 * Time：18:42
 * Description：
 *
 * @author ChallenZhang
 * @version 1.0
 */
public class MethodAnnotationContext {
    protected static Map<Method, Annotation> methodAnnotationMap = new ConcurrentHashMap<>();

    @SuppressWarnings("unchecked")
    public static <T> T getAnnotation(Method method) {
        return (T) methodAnnotationMap.get(method);
    }

    public static void putMethodAnnotation(Method method, Annotation annotation) {
        Annotation result = methodAnnotationMap.putIfAbsent(method, annotation);
        if (result != null) {
            throw new RuntimeException("Cache注解重复");
        }
    }

}
