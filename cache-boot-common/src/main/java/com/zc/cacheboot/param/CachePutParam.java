package com.zc.cacheboot.param;

import lombok.Data;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

/**
 * Project：cache-boot
 * Date：2022-03-07
 * Time：22:05
 * Description：CachePut参数
 *
 * @author ChallenZhang
 * @version 1.0
 */
@Data
public class CachePutParam extends CacheParam<Object> {

    public CachePutParam() {
    }

    public CachePutParam(Object bean, Method method, MethodProxy methodProxy, Object[] args, String key, long timeout, TimeUnit unit, String unless) {
        super(bean, method, methodProxy, args, key, timeout, unit, unless);
    }
}
