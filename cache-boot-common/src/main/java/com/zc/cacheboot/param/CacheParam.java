package com.zc.cacheboot.param;

import lombok.Data;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

/**
 * Project：cache-boot
 * Date：2022-03-09
 * Time：21:27
 * Description：基础参数类
 *
 * @author ChallenZhang
 * @version 1.0
 */
@Data
public class CacheParam<T> {

    /**
     * 拦截的bean
     */
    private Object bean;

    /**
     * method
     */
    private Method method;
    /**
     * 代理method
     */
    private MethodProxy methodProxy;

    /**
     * 参数
     */
    private Object[] args;

    /**
     * 缓存key
     */
    private String key;

    /**
     * 超时时间
     */
    private long timeout;

    /**
     * 时间单位
     */
    private TimeUnit unit;

    /**
     * 后置条件
     */
    private String unless;

    public CacheParam() {
    }

    public CacheParam(Object bean, Method method, MethodProxy methodProxy, Object[] args, String key, long timeout, TimeUnit unit, String unless) {
        this.bean = bean;
        this.method = method;
        this.methodProxy = methodProxy;
        this.args = args;
        this.key = key;
        this.timeout = timeout;
        this.unit = unit;
        this.unless = unless;
    }
}
