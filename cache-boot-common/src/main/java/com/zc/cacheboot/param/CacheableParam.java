package com.zc.cacheboot.param;

import lombok.Data;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

/**
 * Project：cache-boot
 * Date：2022-03-09
 * Time：20:54
 * Description：Cacheable的参数
 *
 * @author ChallenZhang
 * @version 1.0
 */
@Data
public class CacheableParam<T> extends CacheParam<T> {

    /**
     * 返回值类型
     */
    private Class<T> clazz;

    /**
     * 是否续期
     */
    private Boolean renewal;

    public CacheableParam() {
    }

    public CacheableParam(Object bean, Method method, MethodProxy methodProxy, Object[] args, String key, long timeout, TimeUnit unit, String unless, Class<T> clazz, Boolean renewal) {
        super(bean, method, methodProxy, args, key, timeout, unit, unless);
        this.clazz = clazz;
        this.renewal = renewal;
    }
}
