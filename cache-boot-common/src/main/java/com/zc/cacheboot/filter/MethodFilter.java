package com.zc.cacheboot.filter;

import com.zc.cacheboot.annotation.CacheAnnotationParser;
import com.zc.cacheboot.constant.CacheAnnotationSet;
import com.zc.cacheboot.listener.MethodAnnotationContext;
import org.springframework.cglib.proxy.CallbackFilter;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * Project：cache-boot
 * Date：2022-03-06
 * Time：18:43
 * Description：拦截方法过滤器
 *
 * @author ChallenZhang
 * @version 1.0
 */
public abstract class MethodFilter implements CallbackFilter {

    private CacheAnnotationParser cacheAnnotationParser;

    public MethodFilter(CacheAnnotationParser cacheAnnotationParser) {
        this.cacheAnnotationParser = cacheAnnotationParser;
    }

    @Override
    public int accept(Method method) {

        for (Class<? extends Annotation> cacheOperationAnnotation : CacheAnnotationSet.getCacheOperationAnnotations()) {
            Annotation annotation = cacheAnnotationParser.findAnnotation(method, cacheOperationAnnotation);
            if (annotation !=null){
                MethodAnnotationContext.putMethodAnnotation(method, annotation);
            }
        }
        return doAccept(method);
    }

    /**
     * 做自己对应的过滤返回
     *
     * @param method method
     * @return 拦截器的索引
     */
    abstract int doAccept(Method method);
}
