package com.zc.cacheboot.parser;

import com.zc.cacheboot.config.CacheBootProperties;
import com.zc.cacheboot.enums.CacheEmptyEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
import org.springframework.core.ParameterNameDiscoverer;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Project：cache-boot
 * Date：2022/3/11
 * Time：14:34
 * Description：表达式解析器
 *
 * @author Challen.Zhang
 * @version 1.0
 */
@Slf4j
public class CacheBootExpressionParser {

    private static Map<String, KeyGenerator> keyGeneratorMap = new ConcurrentHashMap<>();

    private static ParameterNameDiscoverer discoverer = new LocalVariableTableParameterNameDiscoverer();

    private static org.springframework.expression.ExpressionParser parser = new SpelExpressionParser();

    /**
     * 生成缓存key
     */
    public static String generateKey(String expression, String keyGenerator, Object target, Method method, CacheBootProperties cacheBootProperties, Object... params) {
        Object o;
        if (expression == null || "".equals(expression)) {
            o = CacheBootExpressionParser.getKeyGenerator(keyGenerator).generate(target, method, params);
        } else {
            o = CacheBootExpressionParser.parseKey(expression, method, params);
            if (o == null) {
                o = CacheEmptyEnum.EMPTY_KEY.getVal();
            }
        }
        return cacheBootProperties.getUsePrefix() != null && Boolean.TRUE.equals(cacheBootProperties.getUsePrefix()) ? cacheBootProperties.getPrefix() + o : o.toString();
    }

    /**
     * 解析key
     *
     * @return 解析SpEL生成key
     */
    public static Object parseKey(String expression, Method method, Object... params) {
        return parseExpression(expression, method, params, Object.class);
    }

    /**
     * 解析条件
     *
     * @return 解析SpEL返回条件是否成立
     */
    public static boolean parseCondition(String expression, Method method, Object... params) {
        if ("".equals(expression)) {
            return true;
        }
        return parseExpression(expression, method, params, Boolean.class);
    }

    public static <T> T parseExpression(String expression, Method method, Object[] params, Class<T> tClass) {
        String[] parameterNames = discoverer.getParameterNames(method);
        EvaluationContext context = new StandardEvaluationContext();
        if (parameterNames == null) {
            Parameter[] parameters = method.getParameters();
            for (int i = 0, j = parameters.length; i < j; i++) {
                context.setVariable(parameters[i].getName(), params[i]);
            }
        } else {
            for (int i = 0, len = Objects.requireNonNull(parameterNames).length; i < len; i++) {
                context.setVariable(parameterNames[i], params[i]);
            }
        }
        try {
            Expression expressionResult = parser.parseExpression(expression);
            return expressionResult.getValue(context, tClass);
        } catch (Exception e) {
            log.error("解析el表达式失败,Exception : ", e);
            throw e;
        }
    }

    public static Boolean parseResultExpression(String expression, Object param) {
        EvaluationContext context = new StandardEvaluationContext();
        context.setVariable("result", param);
        try {
            Expression expressionResult = parser.parseExpression(expression);
            return expressionResult.getValue(context, Boolean.class);
        } catch (Exception e) {
            log.error("解析el表达式失败,Exception : ", e);
            throw e;
        }
    }

    public static boolean checkUnless(String expression, Object obj) {
        if ("".equals(expression) || !parseResultExpression(expression, obj)) {
            return true;
        }
        return false;
    }

    public static void put(String beanName, KeyGenerator keyGenerator) {
        keyGeneratorMap.put(beanName, keyGenerator);
    }

    public static KeyGenerator getKeyGenerator(String keyGenerator) {
        return keyGeneratorMap.get(keyGenerator);
    }

    public static void putAll(Map<String, KeyGenerator> map) {
        keyGeneratorMap.putAll(map);
    }
}
