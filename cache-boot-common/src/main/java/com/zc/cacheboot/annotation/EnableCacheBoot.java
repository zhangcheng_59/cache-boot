package com.zc.cacheboot.annotation;

import com.zc.cacheboot.config.CacheBootAutoConfiguration;
import com.zc.cacheboot.config.CacheBootTypeSelector;
import com.zc.cacheboot.enums.CacheTypeEnum;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * Project：cache-boot
 * Date：2022-03-05
 * Time：9:00
 * Description：开启缓存注解
 *
 * @author ChallenZhang
 * @version 1.0
 */
@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Import({CacheBootTypeSelector.class, CacheBootAutoConfiguration.class})
public @interface EnableCacheBoot {

    /**
     * @return 缓存类型
     */
    CacheTypeEnum cacheType() default CacheTypeEnum.REDIS;

    /**
     * @return 默认缓存key生成器
     */
    String keyGenerator() default "";

}
