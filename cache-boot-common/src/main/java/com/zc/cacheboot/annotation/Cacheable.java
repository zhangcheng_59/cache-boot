package com.zc.cacheboot.annotation;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * Project：cache-boot
 * Date：2022-03-05
 * Time：9:27
 * Description：缓存注解,将方法返回值缓存
 *
 * @author ChallenZhang
 * @version 1.0
 */
@Documented
@Inherited
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Cacheable {

    /**
     * @return 缓存key, 支持SpEL表达式
     */
    String key() default "";

    /**
     * @return 缓存条件, 符合缓存 方法执行前判断, 需使用SpEL表达式
     */
    String condition() default "";

    /**
     * @return 缓存条件, 不符合缓存 方法执行后判断, 需使用SpEL表达式
     */
    String unless() default "";

    /**
     * @return key生成器, 默认使用{@link org.springframework.cache.interceptor.SimpleKeyGenerator}
     */
    String keyGenerator() default "default";

    /**
     * @return 缓存过期时间 < 0 代表不过期
     */
    long expireTime() default -1;

    /**
     * @return 访问是否续期
     */
    boolean renewal() default false;

    /**
     * @return 过期时间单位
     */
    TimeUnit unit() default TimeUnit.SECONDS;
}
