package com.zc.cacheboot.annotation;

import java.lang.annotation.*;

/**
 * Project：cache-boot
 * Date：2022-03-05
 * Time：11:05
 * Description：移除对应缓存key
 *
 * @author ChallenZhang
 * @version 1.0
 */
@Inherited
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface CacheEvict {

    /**
     *
     * @return  缓存key,支持SpEL表达式
     */
    String key() default "";
    /**
     *
     * @return  缓存条件,方法执行前判断,需使用SpEL表达式
     */
    String condition() default "";

    /**
     *
     * @return  key生成器,默认使用{@link org.springframework.cache.interceptor.SimpleKeyGenerator}
     */
    String keyGenerator() default "default";

    /**
     *
     * @return  是否执行之前移除
     */
    boolean beforeInvocation() default false;

}
