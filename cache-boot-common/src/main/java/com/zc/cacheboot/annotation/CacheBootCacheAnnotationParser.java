package com.zc.cacheboot.annotation;


import com.zc.cacheboot.constant.CacheAnnotationSet;
import org.springframework.core.annotation.AnnotationUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * Project：cache-boot
 * Date：2022-03-05
 * Time：11:05
 * Description：cache-boot实现的注解解析器
 *
 * @author ChallenZhang
 * @version 1.0
 */
public class CacheBootCacheAnnotationParser implements CacheAnnotationParser {


    @Override
    public boolean isCandidateClass(Class<?> targetClass) {
        return AnnotationUtils.isCandidateClass(targetClass, CacheAnnotationSet.getCacheOperationAnnotations());
    }

    @Override
    public boolean isCandidateClassInMethod(Class<?> targetClass) {
        Method[] methods = targetClass.getMethods();
        for (Method method : methods) {
            for (Class<? extends Annotation> cacheOperationAnnotation : CacheAnnotationSet.getCacheOperationAnnotations()) {
                Annotation annotation = findAnnotation(method, cacheOperationAnnotation);
                if (annotation != null) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public <T extends Annotation> T findAnnotation(Method method, Class<T> tClass) {
        return AnnotationUtils.findAnnotation(method, tClass);
    }

}
