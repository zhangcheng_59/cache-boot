package com.zc.cacheboot.annotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * Project：cache-boot
 * Date：2022-03-05
 * Time：11:03
 * Description：注解解析器
 *
 * @author ChallenZhang
 * @version 1.0
 */
public interface CacheAnnotationParser {


    /**
     * 解析目标类是否有cache注解
     *
     * @param targetClass class对象
     * @return 是否存在
     */
    boolean isCandidateClass(Class<?> targetClass);

    /**
     * 解析类里面的方法是否有cache注解
     *
     * @param targetClass class对象
     * @return 是否存在
     */
    boolean isCandidateClassInMethod(Class<?> targetClass);

    /**
     * 获取method上对应类型的注解
     *
     * @param method method对象
     * @param tClass class对象
     * @param <T>    注解类型的泛型
     * @return 注解
     */
    <T extends Annotation> T findAnnotation(Method method, Class<T> tClass);
}
