package com.zc.cacheboot.config;

import com.zc.cacheboot.parser.CacheBootExpressionParser;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.cache.interceptor.SimpleKeyGenerator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

/**
 * Project：cache-boot
 * Date：2022-03-05
 * Time：11:38
 * Description：自动装配类
 *
 * @author ChallenZhang
 * @version 1.0
 */
@Configuration
@EnableConfigurationProperties(CacheBootProperties.class)
@ConditionalOnProperty(prefix = "cache-boot.", value = "enabled", matchIfMissing = true)
public class CacheBootAutoConfiguration implements ApplicationContextAware {

    private ApplicationContext applicationContext;


    @PostConstruct
    public void initKeyGeneratorMap() {
        Map<String, KeyGenerator> beansOfType = applicationContext.getBeansOfType(KeyGenerator.class);
        CacheBootExpressionParser.putAll(beansOfType);
        CacheBootExpressionParser.put("default", new SimpleKeyGenerator());
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
