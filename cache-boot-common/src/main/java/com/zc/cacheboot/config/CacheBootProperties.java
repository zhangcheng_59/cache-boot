package com.zc.cacheboot.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Project：cache-boot
 * Date：2022-03-05
 * Time：11:36
 * Description：cache-boot配置
 *
 * @author ChallenZhang
 * @version 1.0
 */
@Data
@ConfigurationProperties(prefix = "cache-boot")
public class CacheBootProperties {
    /**
     * 缓存key前缀
     */
    private String prefix;

    /**
     * 是否允许使用前缀
     */
    private Boolean usePrefix;

    /**
     * 是否缓存空值，防止缓存穿透
     */
    private Boolean cacheEmpty;

    /**
     * 空值
     */
    private String empty;

    /**
     * 随机时间
     */
    private Integer randomTime;

    ///**
    // * 续期
    // */
    //private boolean useRenewal;
}
