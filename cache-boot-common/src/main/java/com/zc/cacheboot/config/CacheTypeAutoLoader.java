package com.zc.cacheboot.config;

import com.zc.cacheboot.enums.CacheTypeEnum;

/**
 * Project：cache-boot
 * Date：2022-03-05
 * Time：11:27
 * Description：不同缓存实现加载接口
 *
 * @author ChallenZhang
 * @version 1.0
 */
public interface CacheTypeAutoLoader{

    /**
     * 返回对应CacheType用以匹配自动装载类
     * @return  返回对应CacheType
     */
    CacheTypeEnum getCacheType();

}
