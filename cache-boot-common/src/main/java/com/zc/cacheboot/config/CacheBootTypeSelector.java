package com.zc.cacheboot.config;

import com.zc.cacheboot.annotation.CacheBootCacheAnnotationParser;
import com.zc.cacheboot.annotation.EnableCacheBoot;
import com.zc.cacheboot.enums.CacheTypeEnum;
import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.annotation.MergedAnnotation;
import org.springframework.core.type.AnnotationMetadata;

import java.util.ServiceLoader;

/**
 * Project：cache-boot
 * Date：2022-03-05
 * Time：9:13
 * Description：缓存类型选择器
 *
 * @author ChallenZhang
 * @version 1.0
 */
public class CacheBootTypeSelector implements ImportSelector {

    @Override
    public String[] selectImports(AnnotationMetadata importingClassMetadata) {
        MergedAnnotation<EnableCacheBoot> enableCacheBootMergedAnnotation = importingClassMetadata.getAnnotations().get(EnableCacheBoot.class);
        CacheTypeEnum cacheType = enableCacheBootMergedAnnotation.getEnum("cacheType", CacheTypeEnum.class);
        ServiceLoader<CacheTypeAutoLoader> load = ServiceLoader.load(CacheTypeAutoLoader.class);
        for (CacheTypeAutoLoader cacheTypeAutoLoader : load) {
            if (cacheTypeAutoLoader.getCacheType() == cacheType) {
                return new String[]{cacheTypeAutoLoader.getClass().getName(), CacheBootCacheAnnotationParser.class.getName()};
            }
        }
        return new String[0];
    }

}
