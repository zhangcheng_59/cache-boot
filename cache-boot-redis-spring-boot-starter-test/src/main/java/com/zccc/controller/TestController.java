package com.zccc.controller;

import com.zccc.pojo.Test;
import com.zccc.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

/**
 * Project：cache-boot
 * Date：2022-03-09
 * Time：21:51
 * Description：
 *
 * @author ChallenZhang
 * @version 1.0
 */
@RestController
public class TestController {

    @Autowired
    private TestService testService;


    @GetMapping("/{id}")
    public Object get(@PathVariable Long id) {
        return testService.get(id);
    }

    @PutMapping("/{id}")
    public Object insert(@PathVariable Long id) {
        return testService.insert(new Test(id, UUID.randomUUID().toString()));
    }

    @DeleteMapping("/{id}")
    public Object del(@PathVariable Long id) {
        return testService.del(id);
    }
}
