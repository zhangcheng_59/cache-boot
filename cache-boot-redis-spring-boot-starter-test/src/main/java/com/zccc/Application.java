package com.zccc;

import com.zc.cacheboot.annotation.EnableCacheBoot;
import com.zc.cacheboot.enums.CacheTypeEnum;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Project：cache-boot
 * Date：2022-03-09
 * Time：21:44
 * Description：
 *
 * @author ChallenZhang
 * @version 1.0
 */
@EnableCacheBoot(cacheType = CacheTypeEnum.REDIS)
@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
