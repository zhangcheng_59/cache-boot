package com.zccc.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Project：cache-boot
 * Date：2022-03-09
 * Time：21:57
 * Description：
 *
 * @author ChallenZhang
 * @version 1.0
 */
@Configuration
public class RedissonConfig {
    @Bean
    public RedissonClient redissonClient() {
        Config config = new Config();
        config.useSingleServer()
                .setAddress("redis://127.0.0.1:6379")
                .setPassword("Root1234!");

        return Redisson.create(config);
    }
}
