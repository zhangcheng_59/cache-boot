package com.zccc.service;

import com.zc.cacheboot.annotation.CacheEvict;
import com.zc.cacheboot.annotation.CachePut;
import com.zc.cacheboot.annotation.Cacheable;
import com.zccc.pojo.Test;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * Project：cache-boot
 * Date：2022-03-09
 * Time：21:46
 * Description：
 *
 * @author ChallenZhang
 * @version 1.0
 */
public interface TestService {


    @CachePut(key = "#test.id", expireTime = 5, unit = TimeUnit.SECONDS, condition = "", unless = "#result.id <6")
     Test insert(Test test);

    @Cacheable(key = "#id", expireTime = 500, unit = TimeUnit.SECONDS, renewal = true)
     Test get(Long id) ;


    @CacheEvict(key = "#id")
     Test del(Long id) ;
}
