package com.zccc.service;

import com.zccc.pojo.Test;
import org.springframework.stereotype.Service;

/**
 * Project：cache-boot
 * Date：2022/3/11
 * Time：16:35
 * Description：
 *
 * @author Challen.Zhang
 * @version 1.0
 */
@Service
public final class TestServiceImpl implements TestService {
    @Override
    public Test insert(Test test) {
        return test;
    }

    @Override
    public Test get(Long id) {
        return new Test(id, "zc");
    }

    @Override
    public Test del(Long id) {
        return new Test(id, "zc");
    }
}
