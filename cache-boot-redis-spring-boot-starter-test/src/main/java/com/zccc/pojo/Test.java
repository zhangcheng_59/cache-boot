package com.zccc.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Project：cache-boot
 * Date：2022-03-09
 * Time：21:47
 * Description：
 *
 * @author ChallenZhang
 * @version 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Test {

    private Long id;

    private String name;

}
