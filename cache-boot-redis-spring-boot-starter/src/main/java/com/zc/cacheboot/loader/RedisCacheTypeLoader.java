package com.zc.cacheboot.loader;

import com.zc.cacheboot.aspect.RedisCacheEvictMethodInterceptor;
import com.zc.cacheboot.aspect.RedisCachePutMethodInterceptor;
import com.zc.cacheboot.aspect.RedisCacheableMethodInterceptor;
import com.zc.cacheboot.config.CacheBootProperties;
import com.zc.cacheboot.filter.MethodFilter;
import com.zc.cacheboot.filter.RedisMethodFilter;
import com.zc.cacheboot.service.CacheBootOperationService;
import org.redisson.api.RedissonClient;
import org.springframework.cglib.proxy.Callback;
import org.springframework.cglib.proxy.InvocationHandler;
import org.springframework.cglib.proxy.NoOp;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * Project：cache-boot
 * Date：2022-03-05
 * Time：11:31
 * Description：redis自动装配类
 *
 * @author ChallenZhang
 * @version 1.0
 */
public class RedisCacheTypeLoader {

    private final RedisMethodFilter REDIS_METHOD_FILTER;

    private final CacheBootProperties CACHE_BOOT_PROPERTIES;

    private final CacheBootOperationService CACHE_BOOT_OPERATION_SERVICE;

    public RedisCacheTypeLoader(RedisMethodFilter redisMethodFilter, CacheBootProperties cacheBootProperties, CacheBootOperationService cacheBootOperationService) {
        this.REDIS_METHOD_FILTER = redisMethodFilter;
        this.CACHE_BOOT_PROPERTIES = cacheBootProperties;
        this.CACHE_BOOT_OPERATION_SERVICE = cacheBootOperationService;
    }


    public Callback[] buildCacheMethodInterceptors(Object bean) {
        Callback[] callbacks = new Callback[4];
        RedisCacheableMethodInterceptor cacheableMethodInterceptor = new RedisCacheableMethodInterceptor(bean, CACHE_BOOT_PROPERTIES, CACHE_BOOT_OPERATION_SERVICE);
        RedisCachePutMethodInterceptor cachePutMethodInterceptor = new RedisCachePutMethodInterceptor(bean, CACHE_BOOT_PROPERTIES, CACHE_BOOT_OPERATION_SERVICE);
        RedisCacheEvictMethodInterceptor cacheEvictMethodInterceptor = new RedisCacheEvictMethodInterceptor(bean, CACHE_BOOT_PROPERTIES, CACHE_BOOT_OPERATION_SERVICE);
        callbacks[0] = NoOp.INSTANCE;
        callbacks[1] = cacheableMethodInterceptor;
        callbacks[2] = cachePutMethodInterceptor;
        callbacks[3] = cacheEvictMethodInterceptor;
        return callbacks;
    }

    public MethodFilter getMethodFilter() {
        return REDIS_METHOD_FILTER;
    }

    public Callback[] getCallbacks(Object bean) {
        return buildCacheMethodInterceptors(bean);
    }

}
