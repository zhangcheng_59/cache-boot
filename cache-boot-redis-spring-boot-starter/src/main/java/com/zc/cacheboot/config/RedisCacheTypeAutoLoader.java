package com.zc.cacheboot.config;

import com.zc.cacheboot.enums.CacheTypeEnum;
import org.springframework.context.annotation.Import;

/**
 * Project：cache-boot
 * Date：2022-03-07
 * Time：22:33
 * Description：自动装配类
 *
 * @author ChallenZhang
 * @version 1.0
 */
@Import(RedisCacheAutoConfiguration.class)
public class RedisCacheTypeAutoLoader implements CacheTypeAutoLoader{
    @Override
    public CacheTypeEnum getCacheType() {
        return CacheTypeEnum.REDIS;
    }

}
