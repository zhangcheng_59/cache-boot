package com.zc.cacheboot.config;

import com.zc.cacheboot.annotation.CacheAnnotationParser;
import com.zc.cacheboot.aspect.RedisCacheInvocationHandler;
import com.zc.cacheboot.constant.CacheAnnotationSet;
import com.zc.cacheboot.filter.RedisMethodFilter;
import com.zc.cacheboot.listener.MethodAnnotationContext;
import com.zc.cacheboot.loader.RedisCacheTypeLoader;
import com.zc.cacheboot.service.CacheBootOperationService;
import com.zc.cacheboot.service.RedisCacheBootOperationService;
import com.zc.cacheboot.service.RedisService;
import org.redisson.api.RedissonClient;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.SmartInitializingSingleton;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingClass;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Proxy;

/**
 * Project：cache-boot
 * Date：2022-03-07
 * Time：22:34
 * Description：
 *
 * @author ChallenZhang
 * @version 1.0
 */
public class RedisCacheAutoConfiguration implements BeanPostProcessor, SmartInitializingSingleton, ApplicationContextAware {

    private ApplicationContext applicationContext;

    private final Enhancer ENHANCER = new Enhancer();

    @Bean
    @ConditionalOnMissingBean(RedisService.class)
    public RedisService redisService(StringRedisTemplate redisTemplate,
                                     CacheBootProperties cacheBootProperties) {
        return new RedisService(redisTemplate, cacheBootProperties);
    }

    @Bean
    @ConditionalOnMissingBean(value = RedisMethodFilter.class)
    public RedisMethodFilter redisMethodFilter(CacheAnnotationParser cacheAnnotationParser) {
        return new RedisMethodFilter(cacheAnnotationParser);
    }


    @Bean
    @ConditionalOnMissingBean(value = CacheBootOperationService.class)
    public CacheBootOperationService cacheBootOperationService(StringRedisTemplate redisTemplate,
                                                               RedissonClient redissonClient,
                                                               RedisService redisService) {
        return new RedisCacheBootOperationService(redisTemplate, redissonClient, redisService);
    }

    @Bean
    @ConditionalOnMissingBean(value = RedisCacheTypeLoader.class)
    public RedisCacheTypeLoader redisCacheTypeLoader(RedisMethodFilter redisMethodFilter,
                                                     CacheBootProperties cacheBootProperties,
                                                     CacheBootOperationService cacheBootOperationService) {
        return new RedisCacheTypeLoader(redisMethodFilter, cacheBootProperties, cacheBootOperationService);
    }

    @Autowired
    private CacheBootOperationService cacheBootOperationService;

    @Autowired
    private RedisCacheTypeLoader redisCacheTypeLoader;

    @Autowired
    private CacheAnnotationParser cacheAnnotationParser;

    @Autowired
    private CacheBootProperties cacheBootProperties;

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        // 动态代理
        if (!cacheAnnotationParser.isCandidateClassInMethod(bean.getClass())) {
            return bean;
        }
        // final修饰的类不能增强
        if (Modifier.isFinal(bean.getClass().getModifiers())) {
            // 走动态代理
            Class<?>[] interfaces = bean.getClass().getInterfaces();
            if (interfaces.length == 0) {
                return bean;
            }
            for (Class<?> anInterface : interfaces) {
                if (cacheAnnotationParser.isCandidateClass(anInterface)) {
                    for (Method method : anInterface.getMethods()) {
                        for (Class<? extends Annotation> cacheOperationAnnotation : CacheAnnotationSet.getCacheOperationAnnotations()) {
                            Annotation annotation = cacheAnnotationParser.findAnnotation(method, cacheOperationAnnotation);
                            if (annotation != null) {
                                MethodAnnotationContext.putMethodAnnotation(method, annotation);
                            }
                        }
                    }
                }
            }

            return Proxy.newProxyInstance(Thread.currentThread().getContextClassLoader(), interfaces, new RedisCacheInvocationHandler(bean, cacheBootProperties, cacheBootOperationService));
        }
        ENHANCER.setCallbackFilter(getRedisCacheTypeLoader().getMethodFilter());
        ENHANCER.setSuperclass(bean.getClass());
        ENHANCER.setCallbacks(getRedisCacheTypeLoader().getCallbacks(bean));
        return ENHANCER.create();
    }

    @Override
    public void afterSingletonsInstantiated() {
        //    String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
        //    for (String beanDefinitionName : beanDefinitionNames) {
        //        Object bean = applicationContext.getBean(beanDefinitionName);
        //        if (!cacheAnnotationParser.isCandidateClassInMethod(bean.getClass())) {
        //             continue;
        //        }
        //        ENHANCER.setCallbackFilter(getRedisCacheTypeLoader().getMethodFilter());
        //        ENHANCER.setSuperclass(bean.getClass());
        //        ENHANCER.setCallbacks(getRedisCacheTypeLoader().getCallbacks(bean));
        //        Object o = ENHANCER.create();
        //        // 注册进IOC容器
        //        // 动态注册bean  若beanName已存在 spring.main.allow-bean-definition-overriding = true 则替换  否则报错
        //        BeanDefinitionBuilder beanDefinitionBuilder = BeanDefinitionBuilder.genericBeanDefinition(o.getClass());
        //        BeanDefinitionRegistry beanDefinitionRegistry = (BeanDefinitionRegistry) applicationContext;
        //        beanDefinitionRegistry.registerBeanDefinition(beanDefinitionName, beanDefinitionBuilder.getBeanDefinition());
        //
        //}

    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public RedisCacheTypeLoader getRedisCacheTypeLoader() {
        return redisCacheTypeLoader;
    }
}
