package com.zc.cacheboot.aspect;

import com.zc.cacheboot.config.CacheBootProperties;
import com.zc.cacheboot.constant.LockConstant;
import com.zc.cacheboot.param.CachePutParam;
import com.zc.cacheboot.service.CacheBootOperationService;
import com.zc.cacheboot.service.RedisService;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RReadWriteLock;
import org.redisson.api.RedissonClient;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * Project：cache-boot
 * Date：2022-03-05
 * Time：17:32
 * Description：增强CachePut的拦截器
 *
 * @author ChallenZhang
 * @version 1.0
 */
@Slf4j
public class RedisCachePutMethodInterceptor extends AbstractCachePutMethodInterceptor {

    public RedisCachePutMethodInterceptor() {
    }

    public RedisCachePutMethodInterceptor(Object bean) {
        super(bean);
    }

    public RedisCachePutMethodInterceptor(Object bean, CacheBootProperties cacheBootProperties) {
        super(bean, cacheBootProperties);
    }

    public RedisCachePutMethodInterceptor(Object bean, CacheBootProperties cacheBootProperties, CacheBootOperationService cacheBootOperationService) {
        super(bean, cacheBootProperties, cacheBootOperationService);
    }
}
