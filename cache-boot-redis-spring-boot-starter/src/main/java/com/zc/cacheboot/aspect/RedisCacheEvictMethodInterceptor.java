package com.zc.cacheboot.aspect;

import com.zc.cacheboot.config.CacheBootProperties;
import com.zc.cacheboot.service.CacheBootOperationService;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * Project：cache-boot
 * Date：2022-03-05
 * Time：17:32
 * Description：增强CachePut的拦截器
 *
 * @author ChallenZhang
 * @version 1.0
 */
public class RedisCacheEvictMethodInterceptor extends AbstractCacheEvictMethodInterceptor {

    public RedisCacheEvictMethodInterceptor() {
    }

    public RedisCacheEvictMethodInterceptor(Object bean) {
        super(bean);
    }

    public RedisCacheEvictMethodInterceptor(Object bean, CacheBootProperties cacheBootProperties) {
        super(bean, cacheBootProperties);
    }

    public RedisCacheEvictMethodInterceptor(Object bean, CacheBootProperties cacheBootProperties, CacheBootOperationService cacheBootOperationService) {
        super(bean, cacheBootProperties, cacheBootOperationService);
    }
}
