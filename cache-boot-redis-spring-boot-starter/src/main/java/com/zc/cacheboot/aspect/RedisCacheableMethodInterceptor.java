package com.zc.cacheboot.aspect;

import cn.hutool.json.JSONUtil;
import com.zc.cacheboot.config.CacheBootProperties;
import com.zc.cacheboot.enums.CacheEmptyEnum;
import com.zc.cacheboot.param.CacheableParam;
import com.zc.cacheboot.service.CacheBootOperationService;
import com.zc.cacheboot.service.RedisService;
import com.zc.cacheboot.constant.LockConstant;
import org.redisson.api.RLock;
import org.redisson.api.RReadWriteLock;
import org.redisson.api.RedissonClient;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * Project：cache-boot
 * Date：2022-03-05
 * Time：17:32
 * Description：增强CachePut的拦截器
 *
 * @author ChallenZhang
 * @version 1.0
 */
public class RedisCacheableMethodInterceptor extends AbstractCacheableMethodInterceptor {


    public RedisCacheableMethodInterceptor() {
    }

    public RedisCacheableMethodInterceptor(Object bean) {
        super(bean);
    }


    public RedisCacheableMethodInterceptor(Object bean, CacheBootProperties cacheBootProperties) {
        super(bean, cacheBootProperties);
    }

    public RedisCacheableMethodInterceptor(Object bean, CacheBootProperties cacheBootProperties, CacheBootOperationService cacheBootOperationService) {
        super(bean, cacheBootProperties, cacheBootOperationService);
    }
}
