package com.zc.cacheboot.aspect;

import com.zc.cacheboot.config.CacheBootProperties;
import com.zc.cacheboot.service.CacheBootOperationService;

/**
 * Project：cache-boot
 * Date：2022/3/11
 * Time：15:56
 * Description：redis的增强
 *
 * @author Challen.Zhang
 * @version 1.0
 */
public class RedisCacheInvocationHandler extends AbstractBaseCacheInvocationHandler {

    public RedisCacheInvocationHandler() {
    }

    public RedisCacheInvocationHandler(Object bean) {
        super(bean);
    }

    public RedisCacheInvocationHandler(Object bean, CacheBootProperties cacheBootProperties) {
        super(bean, cacheBootProperties);
    }

    public RedisCacheInvocationHandler(Object bean, CacheBootProperties cacheBootProperties, CacheBootOperationService cacheBootOperationService) {
        super(bean, cacheBootProperties, cacheBootOperationService);
    }
}
