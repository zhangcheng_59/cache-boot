package com.zc.cacheboot.filter;

import com.zc.cacheboot.annotation.CacheAnnotationParser;
import com.zc.cacheboot.annotation.CacheEvict;
import com.zc.cacheboot.annotation.CachePut;
import com.zc.cacheboot.annotation.Cacheable;
import com.zc.cacheboot.listener.MethodAnnotationContext;

import java.lang.reflect.Method;

/**
 * Project：cache-boot
 * Date：2022-03-05
 * Time：9:22
 * Description：Redis拦截方法过滤器
 *
 * @author ChallenZhang
 * @version 1.0
 */
public class RedisMethodFilter extends MethodFilter {

    public RedisMethodFilter(CacheAnnotationParser cacheAnnotationParser) {
        super(cacheAnnotationParser);
    }

    @Override
    int doAccept(Method method) {
        if (MethodAnnotationContext.getAnnotation(method) instanceof Cacheable){
            return 1;
        }
        if (MethodAnnotationContext.getAnnotation(method) instanceof CachePut){
            return 2;
        }
        if (MethodAnnotationContext.getAnnotation(method) instanceof CacheEvict){
            return 3;
        }
        return 0;
    }
}
