package com.zc.cacheboot.service;

import cn.hutool.json.JSONUtil;
import com.zc.cacheboot.constant.LockConstant;
import com.zc.cacheboot.enums.CacheEmptyEnum;
import com.zc.cacheboot.param.CachePutParam;
import com.zc.cacheboot.param.CacheableParam;
import org.redisson.api.RLock;
import org.redisson.api.RReadWriteLock;
import org.redisson.api.RedissonClient;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * Project：cache-boot
 * Date：2022/3/11
 * Time：14:00
 * Description：
 *
 * @author Challen.Zhang
 * @version 1.0
 */
public class RedisCacheBootOperationService implements CacheBootOperationService {

    private final StringRedisTemplate redisTemplate;

    private final RedissonClient redissonClient;

    private final RedisService redisService;

    public RedisCacheBootOperationService(StringRedisTemplate redisTemplate, RedissonClient redissonClient, RedisService redisService) {
        this.redisTemplate = redisTemplate;
        this.redissonClient = redissonClient;
        this.redisService = redisService;
    }

    @Override
    public <T> T invoke(CacheableParam<T> cacheableParam) {
        String s = redisTemplate.opsForValue().get(cacheableParam.getKey());
        if (s == null || CacheEmptyEnum.EMPTY_VAL.getVal().equals(s)) {
            RLock lock = redissonClient.getLock(LockConstant.RELOAD_LOCK + cacheableParam.getKey());
            lock.lock();
            try {
                s = redisTemplate.opsForValue().get(cacheableParam.getKey());
                if (s == null || CacheEmptyEnum.EMPTY_VAL.getVal().equals(s)) {
                    RReadWriteLock readWriteLock = redissonClient.getReadWriteLock(LockConstant.UPDATE_LOCK + cacheableParam.getKey());
                    RLock rLock = readWriteLock.readLock();
                    rLock.lock();
                    try {
                        Object invoke;
                        if (cacheableParam.getMethodProxy() == null) {
                            invoke = cacheableParam.getMethod().invoke(cacheableParam.getBean(), cacheableParam.getArgs());
                        } else {
                            invoke = cacheableParam.getMethodProxy().invoke(cacheableParam.getBean(), cacheableParam.getArgs());
                        }
                        return redisService.cache(invoke, cacheableParam);
                    } finally {
                        rLock.unlock();
                    }
                }
                return JSONUtil.toBean(s, cacheableParam.getClazz());
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            } finally {
                lock.unlock();
            }
        }
        if (cacheableParam.getRenewal()) {
            redisTemplate.expire(cacheableParam.getKey(), cacheableParam.getTimeout(), cacheableParam.getUnit());
        }
        return JSONUtil.toBean(s, cacheableParam.getClazz());
    }


    @Override
    public Object invoke(CachePutParam cachePutParam) {
        RReadWriteLock readWriteLock = redissonClient.getReadWriteLock(LockConstant.UPDATE_LOCK + cachePutParam.getKey());
        RLock rLock = readWriteLock.writeLock();
        rLock.lock();
        Object invoke = null;
        try {
            if (cachePutParam.getMethodProxy() == null) {
                invoke = cachePutParam.getMethod().invoke(cachePutParam.getBean(), cachePutParam.getArgs());
            } else {
                invoke = cachePutParam.getMethodProxy().invoke(cachePutParam.getBean(), cachePutParam.getArgs());
            }
            return redisService.cache(invoke, cachePutParam);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        } finally {
            rLock.unlock();
        }
        return invoke;
    }

    @Override
    public void delete(String key) {
        redisTemplate.delete(key);
    }
}
