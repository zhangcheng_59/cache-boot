package com.zc.cacheboot.service;

import cn.hutool.json.JSONUtil;
import com.zc.cacheboot.aspect.AbstractCacheMethodInterceptor;
import com.zc.cacheboot.config.CacheBootProperties;
import com.zc.cacheboot.enums.CacheEmptyEnum;
import com.zc.cacheboot.param.CacheParam;
import com.zc.cacheboot.parser.CacheBootExpressionParser;
import lombok.Data;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.Random;

/**
 * Project：cache-boot
 * Date：2022-03-09
 * Time：21:33
 * Description：redis缓存类
 *
 * @author ChallenZhang
 * @version 1.0
 */
@Data
public class RedisService {

    private StringRedisTemplate redisTemplate;

    private CacheBootProperties cacheBootProperties;

    private Random random = new Random();

    public RedisService(StringRedisTemplate redisTemplate, CacheBootProperties cacheBootProperties) {
        this.redisTemplate = redisTemplate;
        this.cacheBootProperties = cacheBootProperties;
    }

    public <T> T cache(Object invoke, CacheParam<T> cacheParam) {
        String val;
        if (invoke != null) {
            val = JSONUtil.toJsonStr(invoke);
        } else {
            if (!cacheBootProperties.getCacheEmpty()) {
                return null;
            }
            val = CacheEmptyEnum.EMPTY_VAL.getVal();
        }
        if (CacheBootExpressionParser.checkUnless(cacheParam.getUnless(), invoke)) {
            if (cacheParam.getTimeout() < 0) {
                redisTemplate.opsForValue().set(cacheParam.getKey(), val);
            } else {
                long timeout = cacheParam.getTimeout();
                if (cacheBootProperties.getRandomTime() != null && cacheBootProperties.getRandomTime() > 0) {
                    timeout = timeout + random.nextInt(cacheBootProperties.getRandomTime());
                }
                redisTemplate.opsForValue().set(cacheParam.getKey(), val, timeout, cacheParam.getUnit());
            }
        }
        return (T) invoke;
    }


}
