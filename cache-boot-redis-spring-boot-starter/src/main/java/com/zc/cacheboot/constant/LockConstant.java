package com.zc.cacheboot.constant;

/**
 * Project：cache-boot
 * Date：2022/3/8
 * Time：10:56
 * Description：锁的key
 *
 * @author Challen.Zhang
 * @version 1.0
 */
public class LockConstant {

    public static final String UPDATE_LOCK = "UPDATE_LOCK:";

    public static final String RELOAD_LOCK = "RELOAD_LOCK:";

}
